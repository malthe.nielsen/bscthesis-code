import numpy as np
from matplotlib import pyplot as plt
from scipy.signal import find_peaks
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
import sys

#  data_4 = np.genfromtxt('arnold_4.txt', delimiter = ',')
data_47 = np.genfromtxt('arnold_2.txt', delimiter = ',')
#  data_2 = np.genfromtxt('arnold_2.txt', delimiter = ',')
#  data_1 = np.genfromtxt('arnold_1.txt', delimiter = ',')
arn_4 = np.zeros((40, 500))
arn_47 = np.zeros((40, 500))
arn_2 = np.zeros((40, 500))
arn_1 = np.zeros((40, 500))

plt.style.use('goodplot')

nt = 0


for iA in range(40):
    da = data_47[500*iA:500*(1+iA), :]
    dat = da[0, 1::2]
    tim = da[0, ::2]
    inds, _ = find_peaks(dat)
    inds = inds[len(inds)-10 :]
    if iA == 0:
        f0 = np.mean(np.diff(tim[inds])) / (2*np.pi)
        nt = f0
        print(f0)
    ome = 0.0001
    omedt = 0.01
    for i0 in range(500):
        dat = da[i0, 1::2]
        tim = da[i0, ::2]
        inds, _ = find_peaks(dat)
        inds = inds[len(inds)-5 :]

        fi0 = np.mean(np.diff(tim[inds])) / (2*np.pi/ome)
        arn_47[iA,i0] = fi0

        ome += omedt

#  for iA in range(40):
#      da = data_1[500*iA:500*(1+iA), :]
#      dat = da[0, 1::2]
#      tim = da[0, ::2]
#      inds, _ = find_peaks(dat)
#      inds = inds[len(inds)-10 :]
#      if iA == 0:
#          f0 = np.mean(np.diff(tim[inds])) / (2*np.pi)
#          nt = f0
#          print(f0)
#      ome = 0.0001
#      omedt = 0.01
#      for i0 in range(500):
#          dat = da[i0, 1::2]
#          tim = da[i0, ::2]
#          inds, _ = find_peaks(dat)
#          inds = inds[len(inds)-5 :]
#
#          fi0 = np.mean(np.diff(tim[inds])) / (2*np.pi/ome)
#          arn_1[iA,i0] = fi0
#
#          ome += omedt

#  for iA in range(40):
#      da = data_4[500*iA:500*(1+iA), :]
#      dat = da[0, 1::2]
#      tim = da[0, ::2]
#      inds, _ = find_peaks(dat)
#      inds = inds[len(inds)-10 :]
#      if iA == 0:
#          f0 = np.mean(np.diff(tim[inds])) / (2*np.pi)
#          nt = f0
#          print(f0)
#      ome = 0.0001
#      omedt = 0.01
#      for i0 in range(500):
#          dat = da[i0, 1::2]
#          tim = da[i0, ::2]
#          inds, _ = find_peaks(dat)
#          inds = inds[len(inds)-5 :]
#
#          fi0 = np.mean(np.diff(tim[inds])) / (2*np.pi/ome)
#          arn_4[iA,i0] = fi0
#
#          ome += omedt

#  for iA in range(40):
#      da = data_2[500*iA:500*(1+iA), :]
#      dat = da[0, 1::2]
#      tim = da[0, ::2]
#      inds, _ = find_peaks(dat)
#      inds = inds[len(inds)-10 :]
#      if iA == 0:
#          f0 = np.mean(np.diff(tim[inds])) / (2*np.pi)
#          nt = f0
#          print(f0)
#      ome = 0.0001
#      omedt = 0.01
#      for i0 in range(500):
#          dat = da[i0, 1::2]
#          tim = da[i0, ::2]
#          inds, _ = find_peaks(dat)
#          inds = inds[len(inds)-5 :]
#
#          fi0 = np.mean(np.diff(tim[inds])) / (2*np.pi/ome)
#          arn_2[iA,i0] = fi0
#
#          ome += omedt
omega = np.linspace(0,5/0.6596, 500)
#  arnold410 = (arn_4[14, :])
#  arnold210 = (arn_1[4, :])
arnold4710 = (arn_47[30, :])
#  arnold420 = arn_4[7, :]
#  arnold220 = arn_2[7, :]
#  arnold4720 = arn_47[14, :]

fig, plot = plt.subplots(1,2, figsize = (20,10))
#  plt.imshow(np.flip(np.flip(arn_4),1), extent = [0, 5/0.6596 , 0,  6.28], aspect = 'auto',cmap = 'RdYlGn', alpha = 1)
#  plt.xlabel('$\Omega$')
#  plt.ylabel('$\phi$')
#  plt.title('K4 phase shifted')
#  plt.imshow(np.flip(np.flip(arn_4),1), extent = [0, 5/0.6596 , 0,  0.6], aspect = 'auto',cmap = 'RdYlGn', alpha = 0.7)
#  plt.xlabel('$\Omega$')
#  plt.ylabel('K4 $\epsilon$')
#  plt.title('W. and w.out con. K7')
#  plot[0].imshow(np.flip(np.flip(arn),1), extent = [0, 5/0.6596 , 0,  0.6], aspect = 'auto',cmap = 'RdYlGn')
plot[0].imshow(np.flip(np.flip(arn_47),1), extent = [0, 5/0.6596 , 0,  0.6], aspect = 'auto',cmap = 'RdYlGn', alpha = 1)
#  plot[0,0].imshow(np.flip(np.flip(arn_47),1), extent = [0, 5/0.6596 , 0,  0.6], aspect = 'auto',cmap = 'RdYlGn', alpha = 0.6)
plot[0].set_xlabel('Winding number $\Omega$')
plot[0].set_ylabel('External amplitude')
plot[0].set_title('K7 coupled')


plot[1].plot(omega, arnold4710)
#  plot[1].imshow(np.flip(np.flip(arn_47),1), extent = [0, 5/0.6596 , 0,  0.6], aspect = 'auto',cmap = 'Spectral', alpha = 1)
#  plot[1].imshow(np.flip(np.flip(arn_2),1), extent = [0, 5/0.6596 , 0,  0.6], aspect = 'auto',cmap = 'RdYlGn')
plot[1].set_ylabel('Fraction')
plot[1].set_xlabel('Winding number $\Omega$')
plot[1].set_title('Devils staircase at $\epsilon$ = 0.45')

#  plot[2].imshow(np.flip(np.flip(arn_47),1), extent = [0, 5/0.6596 , 0,  0.6], aspect = 'auto',cmap = 'RdYlGn')
#  plot[2].set_ylabel('External amplitude')
#  plot[2].set_xlabel('Winding number $\Omega$')
#  plot[2].set_title('K7 coupled')

#  plot[1,0].plot(omega, arnold210+2)
#  plot[1,0].plot(omega, arnold4710+4)
#  plot[1,0].plot(omega, arnold410)
#  plot[1,0].set_xlabel('Winding number $\Omega$')
#  ticks = ['K4', 'K4+K7', 'K7']
#  plot[1,0].set_yticks([0,2,4])
#  plot[1,0].set_yticklabels(ticks)
#
#  plot[1,1].plot(omega, arnold220+2)
#  plot[1,1].plot(omega, arnold4720+4)
#  plot[1,1].plot(omega, arnold420)
#  plot[1,1].set_xlabel('Winding number $\Omega$')
#

plt.tight_layout()

plt.savefig('lolwhatever.png')


