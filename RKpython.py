import numpy as np
from matplotlib import pyplot as plt
from scipy.signal import find_peaks
import collections
plt.style.use('goodplot')

def f1(X, Z, K1, K2, K3):
    r1 = K1 - K2*Z*X / (K3+X)
    return r1

def f2(X, Y, K4, K5):
    r2 = K4*X**2 - K5*Y
    return r2

def f3(Y, Z, K6, K7):
    r3 = K6*Y - K7*Z
    return r3

k1 = 100; k2 = 0.1; k3 = 10; k40 = 6.0; k5 = 1.0; k6 = 1.0; k70 = 1.0
x = 30; y = 1300; z = 1800
dt = 0.001
RT = 0

N = 300000

f_bin = []
Sav = np.zeros((N, 4))
#  fig, plot = plt.subplots(1,2)
k4 = k40
k7 = k70
for j in range(1):
    y = 1300+ 2.0*j
    z = 1800 - 3.3*j
    x = 30 + .15*j
    RT = 0
    for i in range(N):
        k4 = k40*(1 + 0.2*np.sin(1.5*RT+1));
        #  k7 = k40*(1 + 0.2*np.sin(1.5*RT))

        ka1 = f1(x, z, k1, k2, k3)*dt
        ja1 = f2(x, y, k4, k5)*dt
        la1 = f3(y, z, k6, k7)*dt

        ka2 = f1(x + 0.5*ka1, z + 0.5*la1, k1, k2, k3)*dt
        ja2 = f2(x + 0.5*ka1, y + 0.5*ja1, k4, k5)*dt
        la2 = f3(y + 0.5*ja1, z + 0.5*la1, k6, k7)*dt

        ka3 = f1(x + 0.5*ka2, z + 0.5*la2, k1, k2, k3)*dt
        ja3 = f2(x + 0.5*ka2, y + 0.5*ja2 , k4, k5)*dt
        la3 = f3(y + 0.5*ja2, z + 0.5*la2 , k6, k7)*dt

        ka4 = f1(x + ka3,z + la3 , k1,k2,k3)*dt
        ja4 = f2(x + ka3, y + ja3 , k4,k5)*dt
        la4 = f3(y + ja3, z + la3 , k6,k7)*dt

        x = x + 1./6*(ka1 + 2.*ka2 + 2.*ka3 + ka4)
        y = y + 1./6*(ja1 + 2.*ja2 + 2.*ja3 + ja4)
        z = z + 1./6*(la1 + 2.*la2 + 2.*la3 + la4)


        RT += dt
        Sav[i,0] = RT
        Sav[i,1] = x
        Sav[i,2] = y
        Sav[i,3] = z
    plt.plot(Sav[:,0], Sav[:,1], label = 'NF-kB')
    #  plt.plot(Sav[:,0], 2.2*np.sin(1.5*Sav[:,0]) + np.mean(Sav[:,1]), label = 'K7')
    #  plt.plot(Sav[:,0], 2.2*np.sin(1.5*Sav[:,0] +1) + np.mean(Sav[:,1]), label = 'K4')
    #  plt.plot(Sav[:,0], Sav[:,2], label = 'mRNA', color = 'lightcoral')
    #  plt.plot(Sav[:,0], Sav[:,3], label = 'IkB', color = 'dodgerblue')
    plt.xticks([])
    plt.yticks([])
    plt.xlabel('Time')
    plt.ylabel('[Nf-kB]')
    plt.legend()
    inds, _ = find_peaks(Sav[:,3], height = 1500)
    inds = inds[15:]
    freq = np.mean(np.diff(Sav[:,0][inds]))
    freq = np.around(freq, 2)
    f_bin.append(freq)
    #  plt.plot(Sav[:,0], k40*(1+2*np.sin(0.95 *Sav[:,0])))
    inds = []
    #  for i in range(len(Sav[:,1])-5):
    #      if Sav[i,1] < Sav[i+3,1] < Sav[i+6,1]:
    #          inds.append(i)
    #      else:
    #          continue

    #
    pv, _ = find_peaks(Sav[:,3])
    pt = np.around(Sav[:,3][pv], decimals=-1)
    
    #  print(np.mean(np.diff(pt[len(pt)-5 :])))
    #  plt.scatter(Sav[:,0][pv], Sav[:,3][pv])
    #  c_list = collections.Counter(pt).most_common(3)
    #  if c_list[2][1] > 10 and c_list[1][1] > 10 and c_list[0][1] > 10:
    #      print(3)
    #  elif c_list[1][1] > 10 and c_list[0][1] > 10:
    #      print(2)
    #  elif c_list[0][1] > 10:
    #      print(1)
    #  else:
    #      print(0)


print(collections.Counter(f_bin))
print(f_bin)
plt.tight_layout()
#  plt.plot(Sav[:,1], Sav[:,3])
plt.savefig(f'lolwhatever.png')








