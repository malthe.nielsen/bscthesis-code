import numpy as np
from matplotlib import pyplot as plt
import sys
import nibabel as nib
import os
from scipy import interpolate
import xlsxwriter
import argparse
import re
from pathlib import Path
#  canontime = np.linspace(20,200,22)

#  regions = ['Brain', 'Cervical Spine', 'Thoracic Spine', 'Lumbar Spine', 'Kidneys', 'Bladder and peepad', 'Heart']
#  region_idxs = [1, 2, 3, 4, 6, 7,8]
#  canon_time = (-0.1:21.1)*(minutes(9)+seconds(40))

def load_func(et_file, seg_file, log_file, seg_txt, ID):

    data = np.loadtxt(seg_txt,dtype = str, comments = '#',  delimiter ='tab')
    regions = []
    region_idxs = []
    for i in range(data.shape[0]):
        r = re.compile(r'([^\d]*)\d*')
        dataj = r.findall(str(data[i]))[:-1]
        for j in range(len(dataj)):
            dataj[j] = dataj[j].replace('"', '')
        regions.append(str(dataj[-1]))
        r = re.compile(r'([^\D]*)\D*')
        dataj = r.findall(str(data[i]))[:-1]
        region_idxs.append(int(dataj[1]))


    outdir =os.path.split(et_file)
    outdir = outdir
    name = outdir[1].split('.')
    name = name[0]
    #print(str(outdir[0]), 'out')
    outdir = os.path.join(outdir[0], f'{name}.xlsx')
    #print(outdir)
    #sys.exit()
    
    

    et = nib.load(et_file)
    hdr = et.header
    print(hdr)
    raw = hdr.structarr

    seg = nib.load(seg_file)
    #  blob = nib.load(blob_file)
    

    ifo = np.genfromtxt(log_file, delimiter = ',')
    time = ifo
    
    #  sys.exit()
    #  et = et*hdr.MultiplicativeScaling
    et = et.get_fdata()
    seg = seg.get_fdata()
    canontime = np.arange(0,et.shape[3]*10, 10)
    #  blob = blob.get_fdata()
    #  et_dc = np.zeros_like(et)
    #  for i in range(et.shape[3]):
    #      et_dc[:,:,:,i] = et[:,:,:,i] * np.exp(np.log(2)/(6.0067)*time[i])

    tracer_mass = np.zeros((len(region_idxs),et.shape[3]))
    tracer_avg = np.zeros((len(region_idxs), et.shape[3]))
    #  tracer_mass_blob = np.zeros(et.shape[3])
    #  tracer_avg_blob = np.zeros(et.shape[3])

    et = et/ID*100
    print(et.shape)
    pix_dim = raw['pixdim']
    for i in range(et.shape[3]):
        I = et[:,:,:,i]
        for j in range(len(region_idxs)):
            tracer_mass[j,i] = np.nansum(I[seg == region_idxs[j]]) * np.prod(pix_dim[1:3]/10)
            tracer_avg[j,i] = np.nanmean(I[seg == region_idxs[j]])

        #  tracer_mass_blob[i] = np.nansum(I[blob != 0]) * np.prod(pix_dim[1:3]/10)
        #  tracer_avg_blob[i] = np.nanmean(I[blob != 0])

    print(tracer_mass)
    print(tracer_avg)

    #  canon_tracer_mass = np.zeros((len(region_idxs), et.shape[3]))
    #  canon_tracer_avg = np.zeros((len(region_idxs),et.shape[3]))
    #  canon_tracer_mass_blob = np.zeros(et.shape[3])
    #  canon_tracer_avg_blob = np.zeros(et.shape[3])

    #  for j in range(len(region_idxs)):
    #      k  =  interpolate.interp1d(time, tracer_mass[j,:])
    #      canon_tracer_mass[j,:] = k(canontime)
    #      k = interpolate.interp1d(time, tracer_avg[j,:])
    #      canon_tracer_avg[j,:] = k(canontime)
    #
    #  k  = interpolate.interp1d(time,tracer_mass_blob[:])
    #  canon_tracer_mass_blob = k(canontime)
    #  k  = interpolate.interp1d(time,tracer_avg_blob[:])
    #  canon_tracer_avg_blob = k(canontime)


    #  fname = str(f'{name}.xlsx')
    #  outdir = Path(et_file) / 'fname'
    #
    #  print(outdir)
    #workbook = xlsxwriter.Workbook(f'{name}.xlsx')
    workbook = xlsxwriter.Workbook(outdir)
    worksheet = workbook.add_worksheet()
    regions_avg = np.zeros_like(regions)
    for i in range(len(regions)):
        regions_avg[i] = regions[i] + ' Average'

    rest_labels = ['Time since infusion']
    labels = np.hstack((regions, regions_avg))
    labels = np.hstack((rest_labels, labels))
    print(labels)

    for i in range(len(time)):
        worksheet.write(i+1,0,canontime[i])
        #  worksheet.write(i+1,1,canon_tracer_mass_blob[i])
        #  worksheet.write(i+1,2,canon_tracer_avg_blob[i])

    for i in range(len(labels)):
        worksheet.write(0,i,labels[i])

    len_lab= len(rest_labels)
    mass_lab = len(regions)
    for j in range(tracer_mass.shape[0]):
        for k in range(tracer_mass.shape[1]):
            worksheet.write(k+1, j+len_lab, tracer_mass[j,k])
            worksheet.write(k+1, j+len_lab+mass_lab, tracer_avg[j,k])

    workbook.close()
    print('Done')
    #  return canon_tracer_mass, canon_tracer_avg, canon_tracer_mass_blob, canon_tracer_avg_blob, time
    

#  c_mass, c_avg, blob_mass, blob_avg, time = load_func('/Users/zwx588/Desktop/DATA_for_CM/20200522C_LK_SALINE_CM_FB/20200522C_LK_SALINE_CM_FB_ac_et_stack.nii', '/Users/zwx588/Desktop/DATA_for_CM/20200522C_LK_SALINE_CM_FB/20200522C_LK_SALINE_CM_FB_segmentation.nii', '/Users/zwx588/Desktop/DATA_for_CM/20200522C_LK_SALINE_CM_FB/20200522C_LK_SALINE_CM_FB_segmentation_striatalblob.nii', '/Users/zwx588/Desktop/log.txt', 1389.8)



#  workbook = xlsxwriter.Workbook('Experiment.xlsx')
#  worksheet = workbook.add_worksheet()
#  regions_avg = np.zeros_like(regions)
#  for i in range(len(regions)):
#      regions_avg[i] = regions[i] + ' Average'
#
#  rest_labels = ['Time since infusion', 'Strital', 'Stritral Average']
#  labels = np.hstack((regions, regions_avg))
#  labels = np.hstack((rest_labels, labels))
#  print(labels)
#
#  for i in range(len(time)):
#      worksheet.write(i+1,0,time[i])
#      worksheet.write(i+1,1,blob_mass[i])
#      worksheet.write(i+1,2,blob_avg[i])
#
#  for i in range(len(labels)):
#      worksheet.write(0,i,labels[i])
#
#  for j in range(c_mass.shape[0]):
#      for k in range(c_mass.shape[1]):
#          worksheet.write(k+1, j+3, c_mass[j,k])
#          worksheet.write(k+1, j+10, c_avg[j,k])
#
#  workbook.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='XXX')
    parser.add_argument('et_dir', type = str, help = 'path')
    parser.add_argument('seg_dir', type = str, help = 'path')
    #  parser.add_argument('blob_dir', type = str, help = 'path')
    parser.add_argument('log_dir', type = str, help = 'path')
    parser.add_argument('seg_txt', type = str, help = 'path')
    parser.add_argument('ID', type = float, help = 'path')
    args = parser.parse_args()
    load_func(args.et_dir, args.seg_dir, args.log_dir, args.seg_txt, args.ID)
