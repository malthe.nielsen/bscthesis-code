import numpy as np
from matplotlib import pyplot as plt
from scipy.signal import find_peaks


plt.style.use('goodplot')

data = np.genfromtxt('chaos_2.txt', delimiter = ',')
#  data = np.genfromtxt('chaos_3.txt', delimiter = ',')

xval = np.linspace(0,5,500)
bins = []
fig, plot = plt.subplots(2,2, figsize = (25,15))
for i in range(500):
    std_bin = []
    dat = data[30*i:30*(i+1), 1::2]
    time = data[30*i:30*(i+1), ::2]
    for j in range(30):
        da = dat[j, :]
        tim = time[j, :]
        moedte, anna = find_peaks(da)
        if j == 20 and i== 5 or j == 21 and i == 5: 
            plot[0,1].plot(tim[1:], da)
            plot[0,1].set_xlabel('Time')
            plot[0,1].set_ylabel('IkB')

        if j == 20 and i == 35 or j == 1 and i == 35:
            plot[1,0].plot(tim[1:], da)
            plot[1,0].set_xlabel('Time')
            plot[1,0].set_ylabel('IkB')
            
        if j == 20 and i == 400 or j == 11 and i == 400:
            plot[1,1].plot(tim[1:], da)
            plot[1,1].set_xlabel('Time')
            plot[1,1].set_ylabel('IkB')

        pt = tim[moedte]
        period = np.mean(np.diff(pt[len(pt)-10:]))
        std_bin.append(period)
    
    i_std = np.std(std_bin)
    bins.append(i_std)

ind = np.array([6,230,400])

#  plot[0,0].figure(figsize = (10,10))
plot[0,0].plot(xval, bins, zorder = -1)
plot[0,0].set_xlabel('External amplitude $\epsilon$')
plot[0,0].set_ylabel('$\sigma$ Period')
#  plot[0,0].scatter(xval[6], bins[6], color = 'black', marker = 'x', s = 200)
#  plot[0,0].scatter(xval[35], bins[35], color = 'black', marker = 'x', s = 200)
#  plot[0,0].scatter(xval[400], bins[400], color = 'black', marker = 'x', s = 200, zorder = 1)
fig.suptitle('Chaos in NF-kB system K4 and two K4+K7 compared')
#  plt.tight_layout()

plt.savefig('chaos_show_k4.png')




    




