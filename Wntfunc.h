#include <math.h> 
#include <stdlib.h> 
#include <time.h>
#include <fstream>
#include <ctype.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;

// #define double float

inline double f1(double v1, double v3,double k1,double k2,double k3) {
  double r1;
  r1 = k1 - k2*v3*v1/(v1+k3);
  return r1;
}
inline double f2(double v1, double v2,double k4,double k5) {
  double r2;
  r2 = k4*v1*v1 - k5*v2;
  return r2;
}
inline double f3(double v2, double v3, double k6,double k7, double k8) {
  double r3;
  r3 = k6*v2 - k7*v3/(v3+k8);
  return r3;
}
