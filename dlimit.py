import numpy as np
from matplotlib import pyplot as plt
from scipy.signal import find_peaks
import collections

data = np.genfromtxt('doublelimit.txt', delimiter = ',')

plotfile = np.zeros((30,500))
plt.style.use('goodplot')

for iA in range(30):
    tmp_data = data[500*iA:500*(1+iA), :]
    for i0 in range(500):
        split_data = tmp_data[i0, :]
        pt, _ = find_peaks(split_data)
        pt = np.around(split_data[pt], -1) 
        c_list = collections.Counter(pt).most_common(3)

        if iA == 40 and i0 == 300:
            #  xval = np.linspace(0,150,len(split_data))
            #  plt.plot(xval, split_data)

            print(c_list)
        elif len(c_list) > 3 and c_list[2][1] > 15 and c_list[1][1] > 15 and c_list[0][1] > 15:
            plotfile[iA,i0] = 3
        elif len(c_list) > 2 and c_list[1][1] > 15 and c_list[0][1] > 15:
            plotfile[iA, i0] = 2
        elif len(c_list) > 1 and c_list[0][1] > 15:
            plotfile[iA, i0] = 1
        else:
            plotfile[iA,i0] = 0

        #      print(c_list)
        #  elif len(c_list) > 3 and c_list[2][1] > 12 and c_list[1][1] > 12 and c_list[0][1] > 12:
        #      plotfile[iA,i0] = 3
        #  elif len(c_list) > 2 and c_list[1][1] > 12 and c_list[0][1] > 12:
        #      plotfile[iA, i0] = 2
        #  elif len(c_list) > 1 and c_list[0][1] > 12:
        #      plotfile[iA, i0] = 1
        #  else:
        #      plotfile[iA,i0] = 0

plt.imshow(np.flip(np.flip(plotfile), 1), extent = [0, 5/0.6569, 0, 0.9], aspect = 'auto')
#  plt.colorbar()
plt.xlabel('$\Omega$')
plt.ylabel('$\epsilon$')
plt.tight_layout()
plt.savefig('nfkb_K7_dlim.png')

