#include <math.h> 
#include <stdlib.h> 
#include <time.h>
#include <fstream>
#include <ctype.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
using namespace std;


inline double f1(double v1, double v2,double k1,double k2,double k3, double k4) {
  double r1;
  r1 = k1 - k2*v1 + k3*v1*v1*v2 - k4*v1;
  return r1;
}
inline double f2(double v1, double v2,double k2,double k3) {
  double r2;
  r2 = k2*v1 + k3*v1*v1*v2 ;
  return r2;
}
