import numpy as np
from matplotlib import pyplot as plt
plt.style.use('goodplot')
from scipy import stats

data = np.genfromtxt('lyapunov_2.txt', delimiter = ',')

N = data.shape[0]-1
length = int((data.shape[1]-1) / 2)

print(length)

x_dat = data[:, 1::2]
y_dat = data[:, ::2]



color = ['lemonchiffon', 'pink', 'crimson', 'orangered', 'forestgreen', 'coral', 'steelblue', 'darkseagreen', 'orangered', 'chocolate']
for i in range(3):
    color = np.concatenate((color,color),axis = 0)

print(color)

dist = []
fig, plot = plt.subplots(1,1,figsize = (10,7))
for i in range(N):
    z0 = np.sqrt(2*(0.01*i + 0.01*i))
    if i == N:
        conx = np.concatenate((x_dat[49, :].reshape(1,length), x_dat[1,:].reshape(1,length)), axis = 0)
        cony = np.concatenate((y_dat[49, 1:].reshape(1,length), y_dat[1, 1:].reshape(1,length)), axis = 0)
    else:
        conx = np.concatenate((x_dat[i, :].reshape(1,length), x_dat[i+1,:].reshape(1,length)), axis = 0)
        cony = np.concatenate((y_dat[i, 1:].reshape(1,length), y_dat[i+1, 1:].reshape(1,length)), axis = 0)
    zjx = np.square(np.diff(conx, axis = 0))
    zjy = np.square(np.diff(cony, axis = 0))
    zj = np.concatenate((zjx,zjy), axis = 0)
    zj = np.sum(zj, axis = 0)
    zj = abs(np.sqrt(zj)) / abs(z0)
    dist.append(zj)
    time = np.linspace(0,100,length)
    
    slope, intercept, r_value, p_value, std_err = stats.linregress(time[:400], (zj[:400]))
    plot.plot(time, zj)
    plot.set_yscale('log')
    plot.set_xlabel('Time')
    plot.set_ylabel('Displacement')
    plt.tight_layout()
    #  plot.plot(time, np.exp(slope*time + intercept), zorder = 1)
    #  if i == 30:
    
    #  plot.plot(cony[0,:],conx[0,:])


    




plt.savefig('lyapunov_k4_4.0.png')
