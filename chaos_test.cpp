#include "RKFunc.h"
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>
#include <ctype.h>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <random>
using namespace std;

int main() {

  ostringstream newname; newname << "Data/chaos_"; newname << 2; newname << ".txt";
  ofstream newfile (newname.str().c_str());
  ostringstream peakname; peakname << "Data/Peak_"; peakname << 2; peakname << ".txt";
  ofstream peakfile (peakname.str().c_str());

  double ka1, ka2, ka3, ka4, ja1, ja2, ja3, ja4, la1, la2, la3, la4;
  double k1, k2, k3, k4, k5, k6, k7, k8, Tmax, RT, ts, dt, x, y, z, x0;
  double k40, Ak4, omek4, p1, k70, Ak7, omek7, p2;
  int click;

  for (int i = 0; i < 500; i++) {
    cout << i << endl;
    for (int j = 0; j < 30; j++) {
      k1 = 100;
      k2 = .10;
      k3 = 10;
      k4 = 1;
      k5 = 1;
      k6 = 1;
      k7 = 1;
      

      Tmax = 100;
      RT = 0;
      
      ts = 0.1;
      dt = 0.001;

      RT = 0;
      click = 1;

      x = 30 + .1*j;
      y = 1300 + 1.0*j;
      z = 1800 - 1.8*j/3;

      k70 = k7;
      
      Ak7 = 0.0 + 0.01*i;
      omek7 = 2.7;  // 0.03 + 0.003*i;

      k40 = k4;
      Ak4 = 0.0 + 0.01*i;
      // omek4 = 0.5;  // 0.03 + 0.03*j;
      omek4 = 2.7;  // 0.03 + 0.03*j;

      while (RT < Tmax) {
        RT = RT + dt;

        // k7 = k70*(1 + Ak7*sin(omek7*RT));
        k4 = k40*(1 + Ak4*sin(omek4*RT));

        ka1 = f1(x,z,k1,k2,k3)*dt;
        ja1 = f2(x,y,k4,k5)*dt;
        la1 = f3(y,z,k6,k7)*dt;
        ka2 = f1(x + 0.5*ka1, z+0.5*la1, k1,k2,k3)*dt;
        ja2 = f2(x + 0.5*ka1, y + 0.5*ja1, k4,k5)*dt;
        la2 = f3(y + 0.5*ja1, z + 0.5*la1 , k6,k7)*dt;
        ka3 = f1(x + 0.5*ka2, z+0.5*la2 , k1,k2,k3)*dt;
        ja3 = f2(x + 0.5*ka2, y + 0.5*ja2 , k4,k5)*dt;
        la3 = f3(y + 0.5*ja2, z + 0.5*la2 , k6,k7)*dt;
        ka4 = f1(x + ka3, z + la3 , k1,k2,k3)*dt;
        ja4 = f2(x + ka3, y + ja3 , k4,k5)*dt;
        la4 = f3(y + ja3, z + la3 , k6,k7)*dt;

        x0 = x;

        x = x + 1./6*(ka1 + 2.*ka2 + 2.*ka3 + ka4);
        y = y + 1./6*(ja1 + 2.*ja2 + 2.*ja3 + ja4);
        z = z + 1./6*(la1 + 2.*la2 + 2.*la3 + la4);

       
        if (RT > ts*click) {
          click++; newfile << RT << "," << z << ",";
        }
        if (x > 100 && x0 <= 100 && y < 300) {
          peakfile << x << "," << y << ",";
        }
      }
      newfile << "\n";
      peakfile << "\n";
    }
  }
}
