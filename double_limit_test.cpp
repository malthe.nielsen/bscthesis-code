#include "RKFunc.h"
// #include "Wntfunc.h"
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <fstream>
#include <ctype.h>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <random>
using namespace std;

int main() {

  ostringstream newname; newname << "Data/doublelimit"; newname << ".txt";
  ofstream newfile (newname.str().c_str());

  double ka1, ka2, ka3, ka4, ja1, ja2, ja3, ja4, la1, la2, la3, la4;
  double k1, k2, k3, k4, k5, k6, k7, k8, Tmax, RT, ts, dt, x, y, z, x0;
  double k40, Ak4, omek4, p1, k70, Ak7, omek7, p2;
  double Ome = 100.0;
  int click;

  for (int i = 0; i < 30; i++) {
    cout << i << endl;
    for (int j = 0; j < 500; j++) {
      k1 = 100;
      k2 = 0.10;
      k3 = 10.0;
      k4 = 1.0;
      k5 = 1.0;
      k6 = 1.0;
      k7 = 1.0;
      k8 = 1.0;
      
      // k1 = 50;
      // k2 = 1;
      // k3 = 1000;
      // k4 = 1;
      // k5 = 100;
      // k6 = 0.010;
      // k7 = 2000;
      // k8 = 0.01;

      Tmax = 300;
      RT = 0;
      ts = 0.1;
      dt = 0.01;
      RT = 0;
      click = 1;

      x = 100;
      y = 200;
      z = 200;

      k70 = k7;
      Ak7 = 0.00 + 0.02*i;
      omek7 = 0.0 + 0.01*j;
      p1 =  0.0;
      
      

      k40 = k4;
      Ak4 = 0.0 + 0.02*i;
      omek4 = 0.0 + 0.01*j;
      p2 = 0.0;
      

      while (RT < Tmax) {
        RT = RT + dt;

        k7 = k70*(1 + Ak7*sin(omek7*RT+p1));
        // k4 = k40*(1 + Ak4*sin(omek4*RT+p2));

        ka1 = f1(x,z,k1,k2,k3)*dt;
        ja1 = f2(x,y,k4,k5)*dt;
        la1 = f3(y,z,k6,k7)*dt;
        ka2 = f1(x + 0.5*ka1,z+0.5*la1 , k1,k2,k3)*dt;
        ja2 = f2(x + 0.5*ka1 , y + 0.5*ja1 , k4,k5)*dt;
        la2 = f3(y + 0.5*ja1 , z + 0.5*la1 , k6,k7)*dt;
        ka3 = f1(x + 0.5*ka2,z+0.5*la2 , k1,k2,k3)*dt;
        ja3 = f2(x + 0.5*ka2 , y + 0.5*ja2 , k4,k5)*dt;
        la3 = f3(y + 0.5*ja2 , z + 0.5*la2 , k6,k7)*dt;
        ka4 = f1(x + ka3,z + la3 , k1,k2,k3)*dt;
        ja4 = f2(x + ka3 , y + ja3 , k4,k5)*dt;
        la4 = f3(y + ja3 , z + la3 , k6,k7)*dt;

        // ka1 = f1(x,z,k1,k2,k3)*dt;
        // ja1 = f2(x,y,k4,k5)*dt;
        // la1 = f3(y,z,k6,k7,k8)*dt;
        // ka2 = f1(x + 0.5*ka1, z+0.5*la1 , k1,k2,k3)*dt;
        // ja2 = f2(x + 0.5*ka1 , y + 0.5*ja1 , k4,k5)*dt;
        // la2 = f3(y + 0.5*ja1 , z + 0.5*la1 , k6,k7,k8)*dt;
        // ka3 = f1(x + 0.5*ka2, z+0.5*la2 , k1,k2,k3)*dt;
        // ja3 = f2(x + 0.5*ka2 , y + 0.5*ja2 , k4,k5)*dt;
        // la3 = f3(y + 0.5*ja2 , z + 0.5*la2 , k6,k7,k8)*dt;
        // ka4 = f1(x + ka3,z + la3 , k1,k2,k3)*dt;
        // ja4 = f2(x + ka3 , y + ja3 , k4,k5)*dt;
        // la4 = f3(y + ja3 , z + la3 , k6,k7,k8)*dt;
        // x0 = x;

        x = x + 1./6*(ka1 + 2.*ka2 + 2.*ka3 + ka4);
        y = y + 1./6*(ja1 + 2.*ja2 + 2.*ja3 + ja4);
        z = z + 1./6*(la1 + 2.*la2 + 2.*la3 + la4);

        if (RT > click*ts) {
          click++; newfile << z << ",";
        
        }
      }
      newfile << "\n";
    }
  }
}
