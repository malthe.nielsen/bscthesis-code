import numpy as np
from matplotlib import pyplot as plt
from scipy.signal import find_peaks
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
import sys
data = np.genfromtxt('devilplat_2.txt', delimiter = ',')


print(data.shape)

arn = np.zeros((200, 150))

plt.style.use('ggplot')



for iA in range(200):
    da = data[150*iA:150*(1+iA), :]
    #  dat = da[0, 1::2]
    #  tim = da[0, ::2]
    #  inds, _ = find_peaks(dat)
    #  inds = inds[len(inds)-10 :]
    #  if iA == 0:
    #  f0 = np.mean(np.diff(tim[inds])) / (2*np.pi)
    #  print(f0)
    ome = 0.0001
    omedt = 0.03
    for i0 in range(150):
        dat = da[i0, 1::2]
        tim = da[i0, ::2]
        inds, _ = find_peaks(dat)
        inds = inds[len(inds)-10 :]
        
        fi0 = np.mean(np.diff(tim[inds])) / (2*np.pi/ome)
        arn[iA,i0] = fi0

        ome += omedt


omega = np.linspace(0,2*np.pi, 500)
arnold = (arn[10, :])


#  fig, plot = plt.subplots(1,2, figsize = (20,20))
plt.imshow(np.flip(np.flip(arn),1), extent = [0, 4.5 , 0, 0.5], aspect = 'auto',cmap = 'RdYlGn')
plt.xlabel('winding number')
plt.ylabel('k4')
plt.colorbar()


#  plt.plot(omega, arnold)
#  plt.set_ylabel('External amplitude')
#  plt.set_xlabel('External period / unpertubed period')
#
#  ax_2 = plt.subplot(gs[2,2])
#  ax_2.plot(time_1[1:], data_1)
#
#  ax_3 = plt.subplot(gs[1,2])
#  ax_3.plot(time_1[1:], data_1)
#
#  ax_4 = plt.subplot(gs[0,2])
#  ax_4.plot(time_1[1:], data_1)
#
plt.tight_layout()

#  print(arnold)

plt.savefig('devilsplateu_wery_strong_k7.png')


